//+------------------------------------------------------------------+
//|                                                         algo.mq4 |
//|                        Copyright 2022, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
int t=0;
double price;
double HIGH = 0.0;
double LOW = 10000.0;

#define MAGICMA  20221113

int OnInit()
  {
   return(INIT_SUCCEEDED);
  }
  
void OnDeinit(const int reason)
  {
  }
  
void OnTick()
  {
   //--- check for history and trading
   if(Bars<100 || IsTradeAllowed()==false)
      return;
//--- calculate open orders by current symbol
   if(CalculateCurrentOrders(Symbol())==0) {t = CheckForOpen();}
   else if (t!=0) {CheckForClose(t);}
   else{printf("error");}
  }

//+------------------------------------------------------------------+
//| Check for open order conditions                                  |
//+------------------------------------------------------------------+

int CheckForOpen()
  {
      double resistance;
      double support;
      double av_mom;
      int InpDepth=12;
      int InpDeviation=5;
      int InpBackstep=4;
      double sar = iSAR(NULL,PERIOD_M5,0.02,0.2,0);
      //--- go trading only for first tiks of new bar
      if(Volume[0]>1) return 0;
      //--- get support and resistance of S&R indicator
      support = iCustom(NULL,PERIOD_CURRENT,"S&R",InpDepth,InpDeviation,InpBackstep,0,0);
      resistance = iCustom(NULL,PERIOD_CURRENT,"S&R",InpDepth,InpDeviation,InpBackstep,1,0);
      av_mom = iCustom(NULL,PERIOD_CURRENT,"Average Momentum",0,0);
      //--- sell conditions
      bool c1 = True;
      bool c2 = Close[2]<support;
      bool c3 = High[1]-Low[1]>1.2*av_mom;
      bool c4 = High[2]-Low[2]>1.2*av_mom;
      bool c5 = MathAbs(Close[1]-Open[1])>0.4*(High[1]-Low[1]);
      bool c6 = MathAbs(Close[2]-Open[2])>0.4*(High[2]-Low[2]);
      bool c7 = True;//Open[2]-7*((Open[2]-Close[2])/10.0)<support;
      bool c8 = Open[1]>Close[1] && Open[2]>Close[2];
      bool c10 = Hour()>9 && Hour()<15;
      bool c12 = Open[1]-Close[1]>7/pow(10,MarketInfo(NULL,MODE_DIGITS)-1) && Open[2]-Close[2]>7/pow(10,MarketInfo(NULL,MODE_DIGITS)-1); //
      
      int j=0;
      double support_long = support;
      while (support_long>Close[2]){
         j+=1;
         support_long = iCustom(NULL,PERIOD_CURRENT,"S&R",InpDepth,InpDeviation,InpBackstep,0,j);      
      }
      
      bool c9 = Close[1]-support_long>15/pow(10,MarketInfo(NULL,MODE_DIGITS)-1); //
      
      j=2;
      HIGH=Close[2];
      while(Close[j]<support){
         HIGH=max(Close[j], HIGH);
         j+=1;
      }
      
      bool c11 = HIGH == Close[2];
  
      if(c1 && c2 && c3 && c4 && c5 && c6 && c7 && c8 && c9 && c10 && c11 && c12) //&& non superposé bougie) et attendre pb ?
        {
         price = Bid;
         int res=OrderSend(Symbol(),OP_SELL,1.0,price,100,sar,0,"",MAGICMA,0,Red); //mofify slippage, stop, tp
         return res;
        }
      //--- buy conditions
      c1 = True;
      c2 = Close[2]>resistance;
      c3 = High[1]-Low[1]>1.2*av_mom;
      c4 = High[2]-Low[2]>1.2*av_mom;
      c5 = MathAbs(Close[1]-Open[1])>0.4*(High[1]-Low[1]);
      c6 = MathAbs(Close[2]-Open[2])>0.4*(High[2]-Low[2]);
      c7 = True;//Open[2]+7*((Close[2]-Open[2])/10.0)>resistance;
      c8 = Close[1]>Open[1] && Close[2]>Open[2];
      c12 = Close[1]-Open[1]>7/pow(10,MarketInfo(NULL,MODE_DIGITS)-1) && Close[2]-Open[2]>7/pow(10,MarketInfo(NULL,MODE_DIGITS)-1); //
      
      
      j=0;
      double resistance_long = resistance;
      while (resistance_long<Close[2]){
         j+=1;
         resistance_long = iCustom(NULL,PERIOD_CURRENT,"S&R",InpDepth,InpDeviation,InpBackstep,1,j);      
      }
      
      c9 = resistance_long-Close[1]>15/pow(10,MarketInfo(NULL,MODE_DIGITS)-1); //
      
      j=2;
      LOW = Close[2];
      while(Close[j]>resistance){
         LOW=max(Close[j], LOW);
         j+=1;
      }
      
      c11 = LOW == Close[2];
      
      if(c1 && c2 && c3 && c4 && c5 && c6 && c7 && c8 && c9 && c10 && c11 && c12)//&& non superposé bougie) et attendre pb ?
        {
         price = Ask;
         int res=OrderSend(Symbol(),OP_BUY,1.0,price,100,sar,0,"",MAGICMA,0,Blue);
         return res;
        }
      return 0;
  }


//+------------------------------------------------------------------+
//| Check for close order conditions                                 |
//+------------------------------------------------------------------+


void CheckForClose(int t)
   {
      double sar = iSAR(NULL,PERIOD_M5,0.02,0.2,0);
      OrderModify(t,price,sar,0,0,CLR_NONE);
   }

/*void CheckForClose()
  {
   double ma;
//--- go trading only for first tiks of new bar
   if(Volume[0]>1) return;
//--- get Moving Average 
   ma=iMA(NULL,0,MovingPeriod,MovingShift,MODE_SMA,PRICE_CLOSE,0);
//---
   for(int i=0;i<OrdersTotal();i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) break;
      if(OrderMagicNumber()!=MAGICMA || OrderSymbol()!=Symbol()) continue;
      //--- check order type 
      if(OrderType()==OP_BUY)
        {
         if(Open[1]>ma && Close[1]<ma)
           {
            if(!OrderClose(OrderTicket(),OrderLots(),Bid,3,White))
               Print("OrderClose error ",GetLastError());
           }
         break;
        }
      if(OrderType()==OP_SELL)
        {
         if(Open[1]<ma && Close[1]>ma)
           {
            if(!OrderClose(OrderTicket(),OrderLots(),Ask,3,White))
               Print("OrderClose error ",GetLastError());
           }
         break;
        }
     }
//---
  }*/
  
  
int CalculateCurrentOrders(string symbol)
  {
   int buys=0,sells=0;
//---
   for(int i=0;i<OrdersTotal();i++)
     {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false) break;
      if(OrderSymbol()==Symbol() && OrderMagicNumber()==MAGICMA)
        {
         if(OrderType()==OP_BUY)  buys++;
         if(OrderType()==OP_SELL) sells++;
        }
     }
//--- return orders volume
   if(buys>0) return(buys);
   else       return(-sells);
  }
  
double max(double a, double b){
   if (a>b) {return a;}
   else {return b;}
}

double min(double a, double b){
   if (a>b) {return b;}
   else {return a;}
}