//+------------------------------------------------------------------+
//|                                             average_momentum.mq4 |
//|                        Copyright 2022, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_separate_window
#property indicator_minimum 0

double Buff[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,Buff);
   SetIndexStyle(0,DRAW_LINE,3,3,clrBlue);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   int limit;
   limit=rates_total-prev_calculated;
   for(int i=0;i<limit;i++)
   {
     if(limit-i-1<rates_total-100){
       double mom=0.0;
       for(int j=0;j<50;j++)
       {
            mom+=MathAbs(iHigh(NULL,PERIOD_CURRENT,limit-i-1+j)-iLow(NULL,PERIOD_CURRENT,limit-i-1+j));
       }
       Buff[i]=mom/50.0;
     }
   }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
