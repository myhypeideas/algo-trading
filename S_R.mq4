//+------------------------------------------------------------------+
//|                                                          S&R.mq4 |
//|                        Copyright 2022, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2022, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 2
#property indicator_plots   2

double support[];
double resistance[];
double prix;
double zigzag1;
double zigzag2;
double supp;
double res;
input int InpDepth=12;
input int InpDeviation=5;
input int InpBackstep=4;
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {
//--- indicator buffers mapping
   SetIndexBuffer(0,support);
   SetIndexStyle(0,DRAW_ARROW,3,3,clrBlue);
   
   SetIndexBuffer(1,resistance);
   SetIndexStyle(1,DRAW_ARROW,3,3,clrRed);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int OnCalculate(const int rates_total,
                const int prev_calculated,
                const datetime &time[],
                const double &open[],
                const double &high[],
                const double &low[],
                const double &close[],
                const long &tick_volume[],
                const long &volume[],
                const int &spread[])
  {
//---
   int limit;
   limit=rates_total-prev_calculated;
   for(int i=0;i<limit;i++)
   {
     if(limit-i-1<rates_total-100){
        int j=limit-i-1;
        zigzag1=iCustom(NULL,PERIOD_CURRENT,"ZigZag",InpDepth,InpDeviation,InpBackstep,0,j);
        prix = iClose(NULL,PERIOD_CURRENT,limit-i-1);
        while(zigzag1==0.0){
            j=j+1;
            zigzag1 = iCustom(NULL,PERIOD_CURRENT,"ZigZag",InpDepth,InpDeviation,InpBackstep,0,j);
        }
        j=j+1;
        zigzag2=iCustom(NULL,PERIOD_CURRENT,"ZigZag",InpDepth,InpDeviation,InpBackstep,0,j);
        while(zigzag2==0.0){
            j=j+1;
            zigzag2 = iCustom(NULL,PERIOD_CURRENT,"ZigZag",InpDepth,InpDeviation,InpBackstep,0,j);
        }
    
       
        if(zigzag1>zigzag2){
            support[limit-i-1]=zigzag2;
            supp=zigzag2;
            resistance[limit-i-1]=res;
        }
        else{
            resistance[limit-i-1]=zigzag2;
            res=zigzag2;
            support[limit-i-1]=supp;
        }
     }
   }
//--- return value of prev_calculated for next call
   return(rates_total);
  }
//+------------------------------------------------------------------+
